from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import User

class RegistrationForm(UserCreationForm):

    email = forms.EmailField(required=True)
    first_name = forms.CharField(max_length=30, required=True, help_text="Ingresa tu nombre.")
    last_name = forms.CharField(max_length=30, required=True, help_text="Ingresa tu apellido")
    dni = forms.CharField(max_length=30, required=True, help_text="Dni")

    class Meta:
        model = User
        fields = ["username", "email", "password1", "password2", "first_name", "last_name", "dni"]

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.is_estudiante = False
        if commit:
            user.save()
        return user

