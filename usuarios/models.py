from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class User(AbstractUser):
    is_estudiante = models.BooleanField(default=False)
    is_profesor = models.BooleanField(default=False)
    is_integrante = models.BooleanField(default=False)
    dni = models.CharField(max_length=12, blank=True, null=True)
    
    class Meta:
        swappable = 'AUTH_USER_MODEL'