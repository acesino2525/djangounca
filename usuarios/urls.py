from django.contrib import admin
from django.urls import path, include
from usuarios import views

urlpatterns = [
    path('', views.home, name='home'),
    path('signup/', views.sign_up),
]