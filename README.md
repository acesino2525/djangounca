# CSTF Django

UPDATE:
- Recuerda instalar los requirements.txt
- Recuerda que necesitas establecer la contraseña de tu postgres en la .env
- Puedes importar la base de datos que adjunto para pruebas, tiene datos ya cargados así como usuarios

Prototipo para la gestión de Proyectos Finales

✅ Creación de modelos

✅ Creación de Views

    - login
    - registro
    - alta alumno
    - alta docente
    - alta integrante
    - carga Proyectos
    - carga alumno a Proyectos
    - carga docente a Proyectos
    - listar Proyectos
    - buscar Proyectos
    - cargar borrador TF
    - asignar fecha para la defensa

✅ Control de views por permisos

✅  Control de views por grupo al que pertenece el usuario

✅ Creación de usuarios en automático al dar el alta como alumno, docente o integrante


-- Login: acesino25, contraseña123 (password, si usas la base de datos que viene precargada)

(https://gitlab.com/acesino2525/djangounca/-/raw/main/1.png)

-- Lo que puedes ver:
Siendo super admin puedes ver todo, sin embargo no podras aprobar un TF o un Proyecto. Los miembros de la 
CSTF pueden tener acceso a cargar alumnos, docentes y otros integrantes de la cstf

(https://gitlab.com/acesino2525/djangounca/-/raw/main/2.png)

Podemos ver todos los proyectos y buscarlos
(https://gitlab.com/acesino2525/djangounca/-/raw/main/3.png)

-- CARGAR UN PROYECTO

La carga de proyecto está dividida en 4. La primera es la carga del proyecto en sí
(https://gitlab.com/acesino2525/djangounca/-/raw/main/4.png)

La segunda es la búsqueda y carga de los integrantes del grupo
(https://gitlab.com/acesino2525/djangounca/-/raw/main/5.png)

La tercera es la carga y designación de los roles de los docentes relacionados al proyecto
(https://gitlab.com/acesino2525/djangounca/-/raw/main/6.png)

🎉🎉🎉 FELICIDADES! has cargado un proyecto!
(https://gitlab.com/acesino2525/djangounca/-/raw/main/7.png)

-- Luego tienes la opción de evaluarlo formalmente, evaluar como tribunal, asignar tribunal, cargar borrador y asignar fecha para la defensa

(https://gitlab.com/acesino2525/djangounca/-/raw/main/8.png)
(https://gitlab.com/acesino2525/djangounca/-/raw/main/9.png)
(https://gitlab.com/acesino2525/djangounca/-/raw/main/10.png)