from django.utils.timezone import now
from django import forms
from .models import EvaluarBorrador, EvaluarTribunal, Tf, Tribunal, Disposicion

class TribunalForm(forms.ModelForm):
    archivo = forms.FileField()

    def __init__(self, *args, idProyecto=None, **kwargs):
        super(TribunalForm, self).__init__(*args, **kwargs)
        self.idProyecto = idProyecto

    class Meta:
        model = Tribunal
        fields = ["presidente", "vocalTitular1", "vocalTitular2", "vocalSuplente1", "vocalSuplente2", "archivo"]

    def save(self, commit=True):
        # Set the idProyecto before saving the tribunal instance
        self.instance.idProyecto = self.idProyecto

        tribunal_instance = super().save(commit)

        archivo_value = self.cleaned_data.get('archivo')

        if archivo_value:
            # Create a Disposicion instance with the tribunal and archivo
            disposicion_instance = Disposicion(tribunal=tribunal_instance, archivo=archivo_value)
            disposicion_instance.save()

        return tribunal_instance
    
class TfForm(forms.ModelForm):
    class Meta:
        model = Tf
        fields = ['fDefensa']

        widgets = {
            'fDefensa': forms.DateInput(attrs={'type': 'date'}),
        }

    def clean_fDefensa(self):
        f_defensa = self.cleaned_data.get('fDefensa')

        # Check if the date is in the past
        if f_defensa and f_defensa.date() < now().date():
            raise forms.ValidationError('Date cannot be in the past.')

        return f_defensa
    
class EvaluarTribunalForm(forms.ModelForm):
    class Meta:
        model = EvaluarTribunal
        fields = ["informe", "estado"]

class EvaluarBorradorForm(forms.ModelForm):
    class Meta:
        model = EvaluarBorrador
        fields = ["informe", "estado"]