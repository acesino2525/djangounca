from django.db import models
from cstf.models import Estado
from proyectos.models import Docente, Proyecto

# Create your models here.
class Tribunal (models.Model):
    presidente = models.ForeignKey(Docente, on_delete=models.PROTECT, related_name='tribunales_presidente')
    vocalTitular1 = models.ForeignKey(Docente, on_delete=models.PROTECT, related_name='tribunales_vocalTitular1')
    vocalTitular2 = models.ForeignKey(Docente, on_delete=models.PROTECT, related_name='tribunales_vocalTitular2')
    vocalSuplente1 = models.ForeignKey(Docente, on_delete=models.PROTECT, related_name='tribunales_vocalSuplente1')
    vocalSuplente2 = models.ForeignKey(Docente, on_delete=models.PROTECT, related_name='tribunales_vocalSuplente2')
    idProyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
class Disposicion (models.Model):
    tribunal = models.ForeignKey(Tribunal, on_delete=models.PROTECT, related_name='disposiciones')
    f_disposicion = models.DateTimeField(auto_now= True, verbose_name= 'Fecha Disposicion')
    archivo = models.FileField(upload_to='disposiciones/')

class EvaluarTribunal(models.Model):
    idProyecto = models.ForeignKey(Proyecto, models.CASCADE, related_name='EvaluarTribunal_proyecto')
    idTribunal = models.ForeignKey(Tribunal, models.CASCADE, related_name='EvaluarTribunal_tribunal')
    informe = models.FileField(upload_to='informesTribunal/')
    estado = models.ForeignKey(Estado, on_delete=models.PROTECT)

class EvaluarBorrador(models.Model):
    idProyecto = models.ForeignKey(Proyecto, models.CASCADE, related_name='EvaluarBorrador_proyecto')
    idTribunal = models.ForeignKey(Tribunal, on_delete=models.CASCADE, related_name='EvaluarBorrador_tribunal')
    estado = models.ForeignKey(Estado, on_delete=models.PROTECT)
    informe = models.FileField(upload_to='borradores/')  

class Tf(models.Model):
    idProyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, related_name='tf_proyecto')
    fDefensa = models.DateTimeField()
    nota = models.FloatField(null=True, blank=True)