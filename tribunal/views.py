from django.shortcuts import get_object_or_404, redirect, render

from proyectos.models import Proyecto, SeguimientoProyecto, Grupo, Conforman
from cstf.models import Estado
from tribunal.models import Tribunal
from .forms import EvaluarBorradorForm, EvaluarTribunalForm, TfForm, TribunalForm
from django.contrib.auth.decorators import login_required, permission_required


# Create your views here.
@login_required(login_url='/login')
def asignar_tribunal(request, id):
    proyecto = Proyecto.objects.get(id=id)
    
    if request.method == "POST":
        print("Ingresa al post)")
        formTribunal = TribunalForm(request.POST, request.FILES, idProyecto=proyecto)
        tribunal = formTribunal.save(commit=False)

        # Assign the existing proyecto instance to the tribunal
        tribunal.idProyecto = proyecto

        tribunal.save()

        return redirect('/')

    else:
        form = TribunalForm()

    return render(request, 'asignar_tribunal.html', {'form': form, 'proyecto': proyecto})

@login_required(login_url='/login')
def asignar_fecha_defensa(request, id):
    if request.method == "POST":
        instancia = None
        form = TfForm(request.POST)
        if form.is_valid():
            instancia = form.save(commit=False)
            instancia.idProyecto = Proyecto.objects.get(id=id)
            instancia.save()
            estado = SeguimientoProyecto.objects.create(idProyecto=Proyecto.objects.get(id=id), estado="Carga del Borrador del Tf")

            return redirect('/')
    else:
        form = TfForm()
    return render(request, 'asginar_fecha_defensa.html', {'form': form})

@login_required(login_url='/login')
def evaluar_tribunal(request, id):

    if request.method == "POST":
        # Proyecto:
        proyecto = Proyecto.objects.get(id=id)

        form = EvaluarTribunalForm(request.POST, request.FILES)
        evaluacion = form.save(commit=False)
        evaluacion.idProyecto = proyecto
        evaluacion.idTribunal = Tribunal.objects.get(idProyecto=proyecto)

        evaluacion.save()
        return redirect('/')
    else:
        estados = Estado.objects.all()

        # De no existir estados creamos
        if not estados:
            Estado.objects.create(estado='Aprobado')
            Estado.objects.create(estado='Revisado')
            Estado.objects.create(estado='Rechazado')
        # Vamos a obtener todo lo necesario y listarlo en un context:

        # Rechazado?
        rechazado = False
        # Proyecto:
        proyecto = Proyecto.objects.get(id=id)

        # Integrantes:
        # 1) obtener grupo:
        grupo = Grupo.objects.get(idProyectoGrupo=proyecto)
        # 2) obtener al grupo:
        conforman = Conforman.objects.filter(idGrupo=grupo)

        alumnos_conforman = [entry.idAlumno for entry in conforman]
        alumnos_nombres = [alumno.nombre for alumno in alumnos_conforman]
        alumnos_apellidos = [alumno.apellido for alumno in alumnos_conforman]
        alumnos_alta = [entry.fechaAlta for entry in conforman]
        alumnos_baja = [entry.fechaBaja for entry in conforman]

        grupoAlumnos = {
            'alumnos': list(zip(alumnos_nombres, alumnos_apellidos, alumnos_alta, alumnos_baja)),
        }

        print(grupoAlumnos)
        # Creamos el form, considera que si fue rechazado no muestres el form
        form = EvaluarTribunalForm()
        # Creamos el context:
        context = {
            'form': form,
            'proyecto': proyecto,
            'grupo': grupoAlumnos,
            'rechazado':  rechazado,
        }
        
        

    return render(request, 'evaluar_tribunal.html', context)

@login_required(login_url='/login')
def evaluar_tf(request, id):

    if request.method == "POST":
        # Proyecto:
        proyecto = Proyecto.objects.get(id=id)

        form = EvaluarBorradorForm(request.POST, request.FILES)
        evaluacion = form.save(commit=False)
        evaluacion.idProyecto = proyecto
        evaluacion.idTribunal = Tribunal.objects.get(idProyecto=proyecto)

        evaluacion.save()
        return redirect('/')
    else:
        estados = Estado.objects.all()

        # De no existir estados creamos
        if not estados:
            Estado.objects.create(estado='Aprobado')
            Estado.objects.create(estado='Revisado')
            Estado.objects.create(estado='Rechazado')
        # Vamos a obtener todo lo necesario y listarlo en un context:

        # Rechazado?
        rechazado = False
        # Proyecto:
        proyecto = Proyecto.objects.get(id=id)

        # Integrantes:
        # 1) obtener grupo:
        grupo = Grupo.objects.get(idProyectoGrupo=proyecto)
        # 2) obtener al grupo:
        conforman = Conforman.objects.filter(idGrupo=grupo)

        alumnos_conforman = [entry.idAlumno for entry in conforman]
        alumnos_nombres = [alumno.nombre for alumno in alumnos_conforman]
        alumnos_apellidos = [alumno.apellido for alumno in alumnos_conforman]
        alumnos_alta = [entry.fechaAlta for entry in conforman]
        alumnos_baja = [entry.fechaBaja for entry in conforman]

        grupoAlumnos = {
            'alumnos': list(zip(alumnos_nombres, alumnos_apellidos, alumnos_alta, alumnos_baja)),
        }

        # Creamos el form, considera que si fue rechazado no muestres el form
        form = EvaluarBorradorForm()
        # Creamos el context:
        context = {
            'form': form,
            'proyecto': proyecto,
            'grupo': grupoAlumnos,
            'rechazado':  rechazado,
        }
        
        

    return render(request, 'evaluar_tf.html', context)