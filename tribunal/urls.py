from django.contrib import admin
from django.urls import path, include
from tribunal import views

urlpatterns = [
    path('asignar_tribunal/<int:id>', views.asignar_tribunal, name='asignar_tribunal'),
    path('asignar_fecha_defensa/<int:id>', views.asignar_fecha_defensa, name='asignar_fecha_defensa'),
    path('evaluar_tribunal/<int:id>', views.evaluar_tribunal, name='evaluar_tribunal'),
    path('evaluar_tf/<int:id>', views.evaluar_tf, name='evaluar_tf'),
]