var myArray = {{ my_array|safe }};
console.log('Initial Array:', myArray);

// Add options to the select dropdown
myArray.forEach(function (obj) {
    var option = document.createElement('option');
    option.value = obj.id;
    option.textContent = obj.name;
    document.getElementById('selectedObject').appendChild(option);
});

// Handle search input
document.getElementById('searchInput').addEventListener('input', function () {
    var inputValue = this.value.toLowerCase();

    var filteredArray = myArray.filter(function (obj) {
        return obj.name.toLowerCase().includes(inputValue);
    });

    // Update options based on the filtered array
    var selectElement = document.getElementById('selectedObject');
    selectElement.innerHTML = '';

    filteredArray.forEach(function (obj) {
        var option = document.createElement('option');
        option.value = obj.id;
        option.textContent = obj.name;
        selectElement.appendChild(option);
    });
});

// Handle form submission
document.getElementById('myForm').addEventListener('submit', function (event) {
    event.preventDefault();

    // Get the selected object ID
    var selectedObjectId = document.getElementById('selectedObject').value;
    console.log('Selected Object ID:', selectedObjectId);

    // Send the array back via a POST request (you can use Fetch API or other methods)
    fetch('/my-view/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': '{{ csrf_token }}'
        },
        body: JSON.stringify({ selectedObjectId: selectedObjectId, myArray: myArray })
    }).then(response => response.json())
    .then(data => console.log('Response:', data));
});