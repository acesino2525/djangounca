from django.conf import settings
from django.db import models
from proyectos.models import Proyecto

class Integrante (models.Model):
    usuario = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    cuil = models.CharField(max_length= 11, null= False, verbose_name= 'Cuil')
    nombre = models.CharField(max_length= 100, null=False,verbose_name='nombre')
    apellido = models.CharField(max_length=100, null=False, verbose_name='apellido')

    '''class Meta:
        db_Table= 'Integrantes'
        verbose_name = 'Integrantes'
        verbose_name_plural = 'integrantes'
        ordering = ['-apellido']'''


class Estado(models.Model):
    estado = models.CharField(max_length=100, null=False, verbose_name='estado')

    def __str__(self):
        return f'{self.estado}'

class Evaluacion(models.Model):
    idProyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    idIntegrante = models.ForeignKey(Integrante, on_delete=models.PROTECT)
    informe = models.FileField(upload_to='informeCstf/')
    estado = models.ForeignKey(Estado, on_delete=models.PROTECT)


    '''class Meta:
        db_Table = 'Evaluaciones'
        verbose_name = 'Evaluacion'
        verbose_name_plural = 'Evaluaciones'
        ordering = ['-idIntegrante']'''

