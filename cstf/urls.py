from django.contrib import admin
from django.urls import path, include
from cstf import views

urlpatterns = [
    path('alta_integrante/', views.alta_integrante, name='alta_integrante'),
    path('cargar_estados/', views.cargar_estado, name='cargar_estado'),
    path('evaluar_cstf/<int:id>', views.evaluar_cstf, name='evaluar'),
]