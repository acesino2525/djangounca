from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from proyectos.models import Conforman, Grupo, Proyecto
from .models import Estado
from .forms import EvaluacionForm, SearchForm, IntegranteForm, EstadoForm
from usuarios.models import User

@login_required(login_url='/login')
def cargar_estado(request):
    if request.method == 'POST':
        form = EstadoForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect("/")
    else:
        form = EstadoForm()
        
    return render(request, 'cargar_rol.html', {"form": form})

@login_required(login_url='/login')
def alta_integrante(request):
    user_first_name = None
    user_last_name = None
    user_id = None

    if request.method == 'POST':
        search_form = SearchForm(request.POST)
        alta_form = IntegranteForm(request.POST)

        if search_form.is_valid():
            user_dni = search_form.cleaned_data['user_dni']
            try:
                user = User.objects.get(dni=user_dni)
                user_first_name = user.first_name
                user_last_name = user.last_name
                user_id = user.id
            except User.DoesNotExist:
                user = None
                user_first_name = None
                user_last_name = None
                user_id = None

        if alta_form.is_valid():
            # GET de la base según user_id
            user_id_from_form = request.POST.get('user_id')

            integrante = alta_form.save(commit=False)
            user = User.objects.get(id=user_id_from_form)
            integrante.usuario = user
            integrante.cuil = alta_form.cleaned_data['cuil']
            integrante.save()

            user.is_integrante = True
            user.save()

            return redirect('/')
    else:
        search_form = SearchForm()
        alta_form = IntegranteForm()

    context = {
        'search_form': search_form,
        'alta_form': alta_form,
        'user_first_name': user_first_name,
        'user_last_name': user_last_name,
        'user_id': user_id,
    }

    return render(request, 'alta_integrante.html', context)

def evaluar_cstf(request, id):

    if request.method == "POST":
        form = EvaluacionForm(request.POST, request.FILES)
        evaluacion = form.save(commit=False)
        evaluacion.idProyecto = Proyecto.objects.get(id=id)
        evaluacion.idIntegrante = request.user

        evaluacion.save()
        return redirect('/')
    else:
        estados = Estado.objects.all()

        # De no existir estados creamos
        if not estados:
            Estado.objects.create(estado='Aprobado')
            Estado.objects.create(estado='Revisado')
            Estado.objects.create(estado='Rechazado')
        # Vamos a obtener todo lo necesario y listarlo en un context:

        # Rechazado?
        rechazado = False
        # Proyecto:
        proyecto = Proyecto.objects.get(id=id)

        # Integrantes:
        # 1) obtener grupo:
        grupo = Grupo.objects.get(idProyectoGrupo=proyecto)
        # 2) obtener al grupo:
        conforman = Conforman.objects.filter(idGrupo=grupo)

        alumnos_conforman = [entry.idAlumno for entry in conforman]
        alumnos_nombres = [alumno.nombre for alumno in alumnos_conforman]
        alumnos_apellidos = [alumno.apellido for alumno in alumnos_conforman]
        alumnos_alta = [entry.fechaAlta for entry in conforman]
        alumnos_baja = [entry.fechaBaja for entry in conforman]

        grupoAlumnos = {
            'alumnos': list(zip(alumnos_nombres, alumnos_apellidos, alumnos_alta, alumnos_baja)),
        }

        print(grupoAlumnos)
        # Creamos el form, considera que si fue rechazado no muestres el form
        form = EvaluacionForm()
        # Creamos el context:
        context = {
            'form': form,
            'proyecto': proyecto,
            'grupo': grupoAlumnos,
            'rechazado':  rechazado,
        }
        
        

    return render(request, 'evaluar_cstf.html', context)