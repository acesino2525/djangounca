from django import forms
from .models import Evaluacion, Integrante, Estado

class SearchForm(forms.Form):
    user_dni = forms.CharField(label='Buscar por DNI', max_length=12)

class IntegranteForm(forms.ModelForm):
    class Meta:
        model = Integrante
        fields = ["cuil"]    


class EstadoForm(forms.ModelForm):
    class Meta:
        model = Estado
        fields = ["estado"]

class EvaluacionForm(forms.ModelForm):

    class Meta:
        model = Evaluacion
        fields = ["informe", "estado"]
