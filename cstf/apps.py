from django.apps import AppConfig


class CstfConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cstf'
