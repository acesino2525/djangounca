from django.db import models
from usuarios.models import User
from django.contrib.auth.models import Group
from django.conf import settings
from django.core.mail import send_mail

#Definimos una funcion para path dinamico
def proyecto_ruta_subida(instance, filename):
    # Es llamada al hacer .save de la instancia en el formulario
    # 
    return f'proyectos/{instance.titulo}/{filename}'

class Proyecto(models.Model):
    titulo = models.CharField(max_length=200)
    descripcion = models.TextField()
    fPresentacion = models.DateTimeField(auto_now_add=True)
    certificadoAnalitico = models.FileField(upload_to=proyecto_ruta_subida)
    notaAceptacion = models.FileField(upload_to=proyecto_ruta_subida)
    curriculumAseso = models.FileField(upload_to=proyecto_ruta_subida)

class Alumno(models.Model):
    usuario = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    matricula = models.IntegerField()
    dni = models.CharField(max_length=12)
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    mail = models.EmailField(max_length=200)

    def save(self, *args, **kwargs):
        if not self.usuario:
            user = User.objects.create_user(
                username = self.dni,
                password = f'{self.apellido}${self.dni}',
                email=self.mail
            )

            self.usuario = user

            subject = 'CSTF - Usuario dado de alta'
            message = f'Hola {self.nombre}, \n\n Tu cuenta ha sido creada. \n Usuario: {self.dni} \n\n contraseña: {self.apellido}${self.dni}. \n\n Una vez loguado cámbiala, por favor'
            from_email = settings.DEFAULT_FROM_EMAIL
            recipient_list = [self.mail]

            send_mail(subject, message, from_email, recipient_list)
        super().save(*args, **kwargs)

class Docente(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    cuil = models.CharField(max_length=20)
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    mail = models.EmailField(max_length=200, null=True)

    def __str__(self):
        return f'{self.nombre} {self.apellido}'

    def save(self, *args, **kwargs):
        if not self.usuario:
            # Check if the user associated with this Docente already exists
            existing_user = User.objects.filter(username=self.cuil).first()

            if existing_user:
                self.usuario = existing_user
            else:
                # If the user doesn't exist, create a new user
                new_user = User.objects.create_user(
                    username=self.cuil,
                    password=f'{self.apellido}${self.cuil}',
                    email=self.mail
                )
                self.usuario = new_user

                # Assign the user to a group (replace 'Docentes' with your group name)
                try:
                    docentes_group = Group.objects.get(name='docente')
                except Group.DoesNotExist:
                    # Create the group if it doesn't exist
                    docentes_group = Group.objects.create(name='docente')

                new_user.groups.add(docentes_group)

                # Send an email to the user with the generated password
                subject = 'CSTF - Usuario dado de alta'
                message = f'Hola {self.nombre}, \n\n Tu cuenta ha sido creada. \n Usuario: {self.cuil} \n\n contraseña: {self.apellido}${self.cuil}. \n\n Una vez loguado cámbiala, por favor'
                from_email = settings.DEFAULT_FROM_EMAIL
                recipient_list = [self.mail]

                send_mail(subject, message, from_email, recipient_list)

        super().save(*args, **kwargs)

class Rol(models.Model):
    rol = models.CharField(max_length=100)

    def __str__(self):
        return self.rol

#Relaciones
class Grupo(models.Model):
    idProyectoGrupo = models.ForeignKey(Proyecto, on_delete=models.CASCADE)

#Relaciones
class BorradorInforme(models.Model):
    idProyectoBorradorInforme = models.OneToOneField(Proyecto, on_delete=models.CASCADE)
    borradorInforme = models.FileField(upload_to='')
    aval = models.FileField(upload_to='')

#Relaciones
class Conforman(models.Model):
    idGrupo = models.ForeignKey(Grupo, on_delete=models.CASCADE)
    idAlumno = models.ForeignKey(Alumno, on_delete=models.PROTECT)
    analitico = models.FileField(upload_to='proyectos/', default='default.pdf', null=True)
    fechaAlta = models.DateTimeField(auto_now_add=True)
    fechaBaja = models.DateTimeField(null=True, blank=True)

#Relaciones
class DocenteProyecto(models.Model):
    idProyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    idDocente = models.ForeignKey(Docente, on_delete=models.SET_NULL, null=True, blank=True)
    rol = models.ForeignKey(Rol, on_delete=models.PROTECT)

#Relaciones
class SeguimientoProyecto(models.Model):
    idProyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    f_estado = models.DateTimeField(auto_now_add=True)
    estado = models.CharField(max_length=100)