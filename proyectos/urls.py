from django.contrib import admin
from django.urls import path, include
from proyectos import views

urlpatterns = [
    path('presentar_proyecto/', views.presentar_proyecto, name='presentar_proyecto'),
    path('presentar_proyecto/cargar_alumnos/', views.cargar_alumnos, name='cargar_alumnos'),
    path('presentar_proyecto/cargar_docentes/', views.cargar_docentes, name='cargar_alumnos'),
    path('check_alumno/', views.check_alumno, name='check_alumno'),
    path('check_docente/', views.check_docente, name='check_docente'),
    path('alta_alumno/', views.alta_alumno, name='alta_alumno'),
    path('buscar_proyecto/', views.buscar_proyecto, name='buscar_proyecto'),
    path('cargar_rol/', views.cargar_rol, name='cargar_rol'),
    path('alta_docente/', views.alta_docente, name='alta_docente'),
    path('cargar_borrador/<int:id>', views.cargar_borrador, name='cargar_borrador'),
]