from django.http import JsonResponse
from django.shortcuts import render, redirect
from cstf.models import Estado
from usuarios.models import User
from .forms import BorradorInformeForm, DocenteProyectoForm, ProyectoForm, AlumnoForm, RolForm, DocenteForm, SearchForm, ConformanForm
from django.contrib.auth.decorators import login_required, permission_required
from .models import DocenteProyecto, Proyecto, Alumno, Grupo, Conforman, SeguimientoProyecto, Rol, Docente
from django.views.decorators.http import require_POST, require_GET
import json

# Create your views here.
@login_required(login_url='/login')
@permission_required("proyectos.add_proyecto", "/")
def presentar_proyecto(request):
    if request.method == 'POST':
        form = ProyectoForm(request.POST, request.FILES)
        if form.is_valid():
            proyecto = form.save(commit=False)
            proyecto.save()

            # Create a new Grupo instance and save it
            grupo = Grupo(idProyectoGrupo=proyecto)
            grupo.save()

            # Get the id of the created grupo
            grupo_id = grupo.id
            proyecto_id = proyecto.id

            # Store the grupo_id in the session
            request.session['grupo_id'] = grupo_id
            request.session['proyecto_id'] = proyecto_id
            print(grupo_id)

            estado = SeguimientoProyecto.objects.create(idProyecto=proyecto, estado="Carga del Proyecto")

            # Redirect to the new URL
            return redirect('/presentar_proyecto/cargar_alumnos/')
    else:
        form = ProyectoForm()
    
    return render(request, 'presentar_proyecto.html', {"form": form})

@require_GET
def check_alumno(request):
    dni = request.GET.get('dni', '')
    alumno = Alumno.objects.filter(dni=dni).first()

    if alumno:
        alumno_data = {
            'alumno_found': True,
            'alumno_id': alumno.id,
        }
    else:
        alumno_data = {'alumno_found': False}

    return JsonResponse(alumno_data)

@login_required(login_url='/login')
def cargar_alumnos(request):
    alumnos = None
    print(request.session['grupo_id'])
    if request.method == 'POST':
        form = ConformanForm(request.POST, request.FILES)

        # Assuming you have an 'alumno_id' field in your ConformanForm
        alumno_id = request.POST.get('alumno_id')
        print(alumno_id)

        if form.is_valid():
            # Retrieve the Alumno instance using alumno_id
            alumno = Alumno.objects.get(id=alumno_id)

            # Assign the Alumno instance to the form's 'alumno' field
            conforman_instance = form.save(commit=False)
            conforman_instance.idAlumno = alumno
            conforman_instance.idGrupo = Grupo.objects.get(id=request.session['grupo_id'])
            conforman_instance.save()
            print(conforman_instance)
            return redirect('/presentar_proyecto/cargar_alumnos/')

    else:
        form = ConformanForm()
        alumnos = list(Conforman.objects.filter(idGrupo=request.session['grupo_id']).select_related('idAlumno'))
        print(alumnos)
    return render(request, 'cargar_alumnos.html', {"form": form, 'alumnos': alumnos})


@require_GET
def check_docente(request):
    cuil = request.GET.get('cuil', '')
    docente = Docente.objects.filter(cuil=cuil).first()

    options_data = list(Rol.objects.all().values())       

    if docente:
        docente_data = {
            'docente_found': True,
            'docente_id': docente.id,
        }
    else:
        docente_data = {'docente_found': False}

    if options_data:
        docente_data['opciones'] = options_data
    else:
        roles = ["director", "co-director", "asesor"]
        rol_objects = [Rol(rol=rolNombre) for rolNombre in roles]

        # Using bulk_create on the model manager
        Rol.objects.bulk_create(rol_objects)

        # Retrieve the roles again
        options_data = list(Rol.objects.all().values())

        docente_data['opciones'] = options_data
        


    return JsonResponse(docente_data)

@login_required(login_url='/login')
def cargar_docentes(request):
    if request.method == 'POST':
        form = DocenteProyectoForm(request.POST)

        # Assuming you have an 'docente_id' field in your ConformanForm
        docente_id = request.POST.get('docente_id')

        if form.is_valid():
            # Retrieve the Alumno instance using alumno_id
            docente = Docente.objects.get(id=docente_id)

            # Asignamos la instancia de docente a la instancia de relación
            docenteProyectoInstance = form.save(commit=False)
            docenteProyectoInstance.idDocente = docente
            docenteProyectoInstance.idProyecto = Proyecto.objects.get(id=request.session['proyecto_id'])
            docenteProyectoInstance.save()

            return redirect('/presentar_proyecto/cargar_docentes/')  # Redirect to the desired URL after processing

    else:
        form = DocenteProyectoForm()
        docentes = list(DocenteProyecto.objects.filter(idProyecto=request.session['proyecto_id']).select_related('idDocente'))
        print(docentes)

    return render(request, 'cargar_docentes.html', {"form": form, 'docentes': docentes})

@login_required(login_url='/login')
@permission_required("proyectos.add_alumno", "/")
def alta_alumno(request):
    if request.method == 'POST':
        form = AlumnoForm(request.POST)
        if form.is_valid():
            # Create an Alumno instance but don't save it yet
            alumno = form.save(commit=False)

            # Check if the user associated with this Alumno already exists
            existing_user = User.objects.filter(username=alumno.dni).first()
            
            if existing_user:
                alumno.usuario = existing_user
            else:
                pass
                # If the user doesn't exist, create a new user
                new_user = User.objects.create_user(
                    username=alumno.dni,
                    password=f'{alumno.apellido}${alumno.dni}',
                    email=alumno.mail
                )
                alumno.usuario = new_user

                # You might want to send an email here as well

            # Save the Alumno instance with the associated user
            alumno.save()

            return redirect('/alta_alumno')

    else:
        form = AlumnoForm()

    return render(request, 'alta_alumno.html', {"form": form})

@login_required(login_url='/login')
def buscar_proyecto(request):
    proyecto_data = []

    # id provisto en la request?
    if 'proyecto_id' in request.GET:
        proyecto_id = request.GET['proyecto_id']
        # Filtramos y consultamos los objetos proyecto en db que coincidan con id
        proyectos = Proyecto.objects.filter(id=proyecto_id)
    else:
        proyectos = Proyecto.objects.all().order_by("-id")

    for proyecto in proyectos:
        # Obtenemos los estados del proyecto
        seguimiento_data = SeguimientoProyecto.objects.filter(idProyecto=proyecto)
        
        # Hacemos objeto ambos y los unimos a un array
        proyecto_data.append({
            'proyecto': proyecto,
            'seguimiento_data': seguimiento_data,
        })

    context = {'proyecto_data': proyecto_data}
    return render(request, 'buscar_proyecto.html', context)

@login_required(login_url='/login')
def cargar_rol(request):
    if request.method == 'POST':
        form = RolForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect("/")
    else:
        form = RolForm()

    return render(request, 'cargar_rol.html', {"form": form})

@login_required(login_url='/login')
@permission_required("proyectos.add_docente", '/')
def alta_docente(request):
    if request.method == 'POST':
        form = DocenteForm(request.POST)
        if form.is_valid():
            docente = form.save(commit=False)

            # Check if the user associated with this Alumno already exists
            existing_user = User.objects.filter(username=docente.cuil).first()
            
            if existing_user:
                docente.usuario = existing_user
            else:
                pass
                # If the user doesn't exist, create a new user
                new_user = User.objects.create_user(
                    username=docente.cuil,
                    password=f'{docente.apellido}${docente.cuil}',
                    email=docente.mail
                )
                docente.usuario = new_user

                # You might want to send an email here as well

            # Save the Alumno instance with the associated user
            docente.save()

            return redirect('/alta_docente')

    else:
        form = DocenteForm()

    return render(request, 'alta_docente.html', {"form": form})

@login_required(login_url='/login')
@permission_required("proyectos.add_borradorinforme", "/")
def cargar_borrador(request, id):
    if request.method == "POST":
        borrador = None
        form = BorradorInformeForm(request.POST, request.FILES)
        print(form)
        if form.is_valid():
            borrador = form.save(commit=False)
            borrador.idProyectoBorradorInforme = Proyecto.objects.get(id=id)
            borrador.save()
            estado = SeguimientoProyecto.objects.create(idProyecto=Proyecto.objects.get(id=id), estado="Carga del Borrador del Tf")
            return redirect('/')
    else:
        form = BorradorInformeForm()

    return render(request, 'cargar_borrador.html', {'form': form})


