from django import forms
from .models import BorradorInforme, DocenteProyecto, Proyecto, Alumno, Rol, Docente, Conforman

class ProyectoForm(forms.ModelForm):
    class Meta:
        model = Proyecto
        fields = ["titulo", "descripcion", "certificadoAnalitico", "notaAceptacion", "curriculumAseso"]

class AlumnoForm(forms.ModelForm):
    class Meta:
        model = Alumno
        fields = ["matricula", "dni", "nombre", "apellido", "mail"]

class RolForm(forms.ModelForm):
    class Meta:
        model = Rol
        fields = ["rol"]

class DocenteForm(forms.ModelForm):
    class Meta:
        model = Docente
        fields = ["cuil", "nombre", "apellido", "mail"]      

class SearchForm(forms.Form):
    user_dni = forms.CharField(label='Buscar por DNI', max_length=12)
        
class ConformanForm(forms.ModelForm):
    dni = forms.CharField(max_length=12, widget=forms.TextInput(attrs={'class': 'form-control'}))
    class Meta:
        model = Conforman
        fields = ["dni", "analitico"]

class DocenteProyectoForm(forms.ModelForm):
    cuil = forms.CharField(max_length=12, widget=forms.TextInput(attrs={'class': 'form-control'}))
    rol = forms.ModelChoiceField(
        queryset=Rol.objects.all(),  # Adjust the queryset to your needs
        widget=forms.Select(attrs={'class': 'form-control'})
    )

    class Meta:
        model = DocenteProyecto
        fields = ["cuil", "rol"]

class BorradorInformeForm(forms.ModelForm):
    class Meta:
        model = BorradorInforme
        fields = ["borradorInforme", "aval"]